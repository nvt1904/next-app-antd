const withLess = require('@zeit/next-less');
const withCss = require('@zeit/next-css');
const withPlugins = require('next-compose-plugins');
const path = require('path');
const generateTheme = require('next-dynamic-antd-theme/plugin');
const { i18n } = require('./next-i18next.config');

const withAntdTheme = generateTheme({
  antDir: path.join(__dirname, './node_modules/antd'),
  stylesDir: path.join(__dirname, './src/themes'),
  varFile: path.join(__dirname, './src/themes/variables.less'),
  outputFilePath: path.join(__dirname, './.next/static/color.less'),
  lessFilePath: `/_next/static/color.less`
});

withAntd = (nextConfig = {}) => {
  return Object.assign({}, nextConfig, {
    ...nextConfig,
    lessLoaderOptions: {
      ...nextConfig.lessLoaderOptions,
      javascriptEnabled: true
    },
    webpack(config, options) {
      if (config.externals) {
        const includes = [/antd/];
        config.externals = config.externals.map((external) => {
          if (typeof external !== 'function') return external;
          return (ctx, req, cb) => {
            return includes.find((include) =>
              req.startsWith('.') ? include.test(path.resolve(ctx, req)) : include.test(req)
            )
              ? cb()
              : external(ctx, req, cb);
          };
        });
      }
      return typeof nextConfig.webpack === 'function'
        ? nextConfig.webpack(config, options)
        : config;
    }
  });
};

const configNext = {
  webpack5: false,
  i18n,
  pageExtensions: ['page.ts', 'page.tsx']
};

module.exports = withPlugins([withLess(withAntdTheme(withAntd(configNext))), withCss]);
