import { useDispatch, useSelector } from 'react-redux';
import { changePage } from 'hooks/base/page/reducer';
import { RootState } from 'pages/store';

function usePage() {
  const page = useSelector((state: RootState) => state.page);
  const dispatch = useDispatch();
  const setPage = (state) => {
    const action = changePage({ ...state });
    dispatch(action);
  };
  return { page, setPage };
}

export default usePage;
