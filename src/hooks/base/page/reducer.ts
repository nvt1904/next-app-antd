import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import _ from 'lodash';
type PageState = {
  isNotFound: boolean;
};

const initialState: PageState = {
  isNotFound: false
};

const page = createSlice({
  name: 'page',
  initialState: initialState,
  reducers: {
    changePage: (state: PageState, action: PayloadAction<PageState>) => {
      state = _.merge(state, action.payload);
    }
  }
});

const { reducer, actions } = page;
export const { changePage } = actions;
export default reducer;
