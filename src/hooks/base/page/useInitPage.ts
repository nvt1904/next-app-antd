import { changePage } from 'hooks/base/page/reducer';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

function useInitPage() {
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    dispatch(changePage({ isNotFound: false }));
  }, [router.pathname]);
}

export default useInitPage;
