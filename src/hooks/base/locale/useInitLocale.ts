import localStorageHelper, { KeyStorage } from 'helper/localStorage';
import moment from 'moment';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import useLocale from './useLocale';

function useInitLocale() {
  const router = useRouter();
  const { locale } = useLocale();
  useEffect(() => {
    if (locale.locale !== router.locale) {
      moment.locale(locale.locale);
      localStorageHelper.setObject(KeyStorage.LOCALE, locale);
      router.replace(router.asPath, router.asPath, { locale: locale.locale });
    }
  }, [locale]);
}

export default useInitLocale;
