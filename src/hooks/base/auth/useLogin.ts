import { IResponse } from 'helper/axiosClient';
import { signIn } from 'next-auth/client';
import { useState } from 'react';

export type DataLogin = {
  username: string;
  password: string;
  redirect?: boolean;
  callbackUrl?: string;
};

function useLogin() {
  const [logging, setLogging] = useState(false);
  const [result, setResult] = useState<IResponse>();
  const login = async (data: DataLogin, type?: string) => {
    setLogging(true);
    const res = await signIn(type, { redirect: false, ...data });
    setResult(res);
    setLogging(false);
    return res;
  };

  return {
    logging,
    login,
    result
  };
}

export default useLogin;
