import { signOut, SignOutParams } from 'next-auth/client';
import { useState } from 'react';

function useLogout() {
  const [loggingOut, setLoggingOut] = useState(false);
  const logout = async (type?: SignOutParams<true>) => {
    setLoggingOut(true);
    signOut(type);
    setLoggingOut(false);
  };

  return {
    loggingOut,
    logout
  };
}

export default useLogout;
