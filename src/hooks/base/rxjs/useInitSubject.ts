import { useEffect } from 'react';
import { Subject } from 'rxjs';

export const subject = new Subject();

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Event = { type: string; data: any };

export default function useInitSubject() {
  useEffect(() => {
    const init = () => {
      subject.subscribe((x: Event) => console.log('subscribe', x));
    };
    init();
    return () => subject.complete();
  }, []);
}
