import locale from './locale/reducer';
import nprogress from './nprogress/reducer';
import page from './page/reducer';
import uiCms from './ui/cms/reducer';
import theme from './ui/theme/reducer';

export default { theme, locale, uiCms, nprogress, page };
