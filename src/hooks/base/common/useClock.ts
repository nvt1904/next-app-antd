import { useInterval } from 'ahooks';
import moment from 'moment';
import { useState } from 'react';

function useClock() {
  const [time, setTime] = useState(moment().add(1, 's'));
  useInterval(() => {
    setTime(moment().add(1, 's'));
  }, 1000);
  return { time };
}

export default useClock;
