import { RootState } from 'pages/store';
import { useDispatch, useSelector } from 'react-redux';
import { changeUiCms, UiCmsState } from './reducer';

function useUiCms() {
  const uiCms = useSelector((state: RootState) => state.uiCms);
  const dispatch = useDispatch();
  const setUiCms = (newUiCms: UiCmsState) => {
    const actionChangeUiCms = changeUiCms(newUiCms);
    dispatch(actionChangeUiCms);
  };

  const toggleSider = (show?: boolean) => {
    setUiCms({ sider: { show: show !== undefined ? show : !uiCms.sider.show } });
  };

  return { uiCms, setUiCms, toggleSider };
}

export default useUiCms;
