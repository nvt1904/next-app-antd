import axiosClient, { IResponse, Params } from 'helper/axiosClient';
import { merge } from 'lodash';
import { useState } from 'react';

export type Address = {
  id: number;
  name: string;
  type: 'province';
  parentId: null | number;
  isFromBooking: boolean;
};

export type ResListAddress = IResponse & {
  items: Address[];
  total: number;
};

function useListAddress() {
  const [loading, setLoading] = useState(false);
  const [params, setParams] = useState({ limit: 10, page: 1, type: 'province' });
  const [result, setResult] = useState<ResListAddress>();

  const onGetListAddress = async (p: Params) => {
    const newParams = merge(params, p);
    setParams(newParams);
    setLoading(true);
    const res = await axiosClient.get<IResponse, ResListAddress>('address', newParams, false);
    if (!res?.error) {
      setResult(res);
    } else {
      setResult(undefined);
    }
    setLoading(false);
    return res;
  };

  const onGetPage = (page = 1) => onGetListAddress({ page });

  return { loading, setLoading, params, setParams, result, setResult, onGetListAddress, onGetPage };
}

export default useListAddress;
