import { merge } from 'lodash';
import { GetStaticProps } from 'next';

export function withRevalidate(
  revalidate = Number(process.env.PAGE_PROPS_REVALIDATE),
  cb?: GetStaticProps
) {
  return async (ctx) => {
    return merge(cb ? await cb(ctx) : {}, { revalidate });
  };
}
