import { GetServerSideProps, GetStaticPaths, GetStaticProps } from 'next';
import { getSession } from 'next-auth/client';

export function withSSUnAuth(cb?: GetServerSideProps | GetStaticProps | GetStaticPaths) {
  return async (context) => {
    const session = await getSession(context);
    if (session) {
      return {
        redirect: {
          destination: context.query?.callbackUrl?.toString() || '/',
          permanent: false
        }
      };
    }
    return cb ? await cb(context) : { props: {} };
  };
}
