import { merge } from 'lodash';
import { GetServerSideProps, GetStaticPaths, GetStaticProps } from 'next';
import { getSession } from 'next-auth/client';
import { stringify } from 'query-string';

export function withSSAuth(cb?: GetServerSideProps | GetStaticProps | GetStaticPaths) {
  return async (ctx) => {
    const session = await getSession(ctx);
    if (!session) {
      return {
        redirect: {
          destination: `/auth/login?${stringify({ callbackUrl: ctx.resolvedUrl })}`,
          permanent: false
        }
      };
    }
    return merge(cb ? await cb(ctx) : {}, { props: { session } });
  };
}
