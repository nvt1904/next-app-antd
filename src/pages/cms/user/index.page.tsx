import dynamic from 'next/dynamic';
import IDatePicker from 'pages/components/base/IDatePicker';
import { withSSAuth } from 'middleware/base/withSSAuth';
import { withSSTranslations } from 'middleware/base/withSSTranslations';
import MasterLayout from '../layouts/MasterLayout';

const IBraftEditor = dynamic(() => import('pages/components/base/IBraftEditor'), { ssr: false });
function IndexUserPage() {
  return (
    <div>
      <IDatePicker />
      <IBraftEditor />
    </div>
  );
}

IndexUserPage.Layout = MasterLayout;

export const getServerSideProps = withSSAuth(withSSTranslations());

export default IndexUserPage;
