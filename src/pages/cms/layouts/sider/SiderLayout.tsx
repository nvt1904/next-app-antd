import { MenuFoldOutlined } from '@ant-design/icons';
import { Button, Drawer, Grid, Layout } from 'antd';
import useUiCms from 'hooks/base/ui/cms/useUiCms';
import React, { useEffect } from 'react';
import MenuSider from './MenuSider';

const { useBreakpoint } = Grid;

const { Sider } = Layout;

function SiderLayout() {
  const { uiCms, toggleSider } = useUiCms();
  const screens = useBreakpoint();
  useEffect(() => {
    if (screens.sm !== undefined) {
      !screens.xl && toggleSider(false);
    }
  }, [screens]);

  const RenderSider = () => {
    return (
      <div style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
        <div
          style={{
            height: `calc(${uiCms.header.height} + ${uiCms.header.margin})`,
            lineHeight: `calc(${uiCms.header.height} + ${uiCms.header.margin})`,
            display: 'flex',
            justifyContent: uiCms.sider.show ? 'space-between' : 'center',
            padding: uiCms.sider.show ? `0 0 0 1em` : '0'
          }}>
          {uiCms.sider.show && <div>Nvt1904</div>}
          <div>
            <Button
              type="link"
              shape="circle"
              onClick={() => toggleSider()}
              icon={<MenuFoldOutlined rotate={uiCms.sider.show ? 0 : 180} />}
            />
          </div>
        </div>
        <MenuSider />
      </div>
    );
  };

  return (
    <>
      {screens.md ? (
        <Sider
          id="sider-bar"
          trigger={null}
          width={uiCms.sider.width}
          style={{
            height: '100vh',
            ...(!screens.md ? { position: 'fixed', zIndex: 10 } : {})
          }}
          collapsedWidth={screens.md ? uiCms.sider.collapsedWidth : 0}
          collapsed={!uiCms.sider.show}>
          <RenderSider />
        </Sider>
      ) : (
        <Drawer
          placement="left"
          closable={false}
          bodyStyle={{ padding: 0 }}
          visible={uiCms.sider.show}
          onClose={() => toggleSider(false)}>
          <RenderSider />
        </Drawer>
      )}
    </>
  );
}

export default React.memo(SiderLayout);
