import { GithubOutlined } from '@ant-design/icons';
import { RowProps } from 'antd/lib/row';
import ElementCenter from 'pages/components/base/ElementCenter';
import React from 'react';

interface Props extends RowProps {
  height?: string | number;
}

function LogoHeader(props: Props) {
  return (
    <ElementCenter {...props}>
      <GithubOutlined style={{ fontSize: '3em', height: '100%' }} />
    </ElementCenter>
  );
}

export default React.memo(LogoHeader);
