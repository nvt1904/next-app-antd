import { Button, Select, Space } from 'antd';
import useListAddress from 'hooks/address/useListAddress';
import { withSSAuth } from 'middleware/base/withSSAuth';
import { withSSTranslations } from 'middleware/base/withSSTranslations';
import { useSession } from 'next-auth/client';
import ChangeLocale from 'pages/components/base/ChangeLocale';
import MasterLayout from './layouts/MasterLayout';

function IndexPage() {
  const { params, onGetListAddress } = useListAddress();
  const [session] = useSession();
  console.log(session);
  return (
    <Space>
      <ChangeLocale />
      <Select>
        <Select.Option value="0">Hello</Select.Option>
        <Select.Option value="1">3</Select.Option>
      </Select>
      <Button
        onClick={() =>
          params?.page > 1 && onGetListAddress({ page: Number(params?.page || 0) - 1 })
        }>
        PrePage
      </Button>
      <Button
        onClick={() => {
          onGetListAddress({ page: Number(params?.page || 0) + 1 });
        }}>
        NextPage
      </Button>
    </Space>
  );
}

IndexPage.Layout = MasterLayout;

export const getServerSideProps = withSSAuth(withSSTranslations());

export default IndexPage;
