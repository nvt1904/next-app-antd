import 'antd/dist/antd.min.css';
import 'styles/global.less';
import { ConfigProvider } from 'antd';
import useInitLocale from 'hooks/base/locale/useInitLocale';
import useLocale from 'hooks/base/locale/useLocale';
import useInitNProgress from 'hooks/base/nprogress/useInitNProgress';
import useInitSubject from 'hooks/base/rxjs/useInitSubject';
import useInitTheme from 'hooks/base/ui/theme/useInitTheme';
import { Provider as AuthProvider } from 'next-auth/client';
import { appWithTranslation, useTranslation } from 'next-i18next';
import type { AppProps } from 'next/app';
import store from 'pages/store';
import { Provider } from 'react-redux';

type Props = {
  children: JSX.Element;
};

function Root(props: Props) {
  useInitSubject();
  useInitTheme();
  useInitLocale();
  useInitNProgress();
  const { t } = useTranslation();
  const { children } = props;
  const { locale } = useLocale();
  return (
    <ConfigProvider
      locale={locale}
      form={{
        validateMessages: t('form:messages', { returnObjects: true })
      }}>
      {children}
    </ConfigProvider>
  );
}

const DefaultLayout = ({ children }) => {
  return <>{children}</>;
};

function App({ Component, pageProps }: AppProps) {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const Layout = (Component as any).Layout || DefaultLayout;
  return (
    <AuthProvider session={pageProps.session}>
      <Provider store={store}>
        <Root>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </Root>
      </Provider>
    </AuthProvider>
  );
}

export default appWithTranslation(App);
