import { configureStore, ConfigureStoreOptions } from '@reduxjs/toolkit';
import baseReducer from 'hooks/base/reducer';
import { createStateSyncMiddleware, initMessageListener } from 'redux-state-sync';

const rootReducer = {
  ...baseReducer
};

const isClient = typeof window !== 'undefined';

const options: ConfigureStoreOptions = {
  reducer: rootReducer,
  devTools: true
};
if (isClient) {
  options.middleware = [
    createStateSyncMiddleware({
      whitelist: ['theme/changeTheme', 'locale/changeLocale', 'uiCms/changeUiCms']
    })
  ];
}

const store = configureStore(options);

if (isClient) {
  initMessageListener(store);
}

export type RootState = ReturnType<typeof store.getState>;
export default store;
