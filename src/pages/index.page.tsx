import useInitSocketIO from 'hooks/base/socket-io/useInitSocketIO';
import { withSSTranslations } from 'middleware/base/withSSTranslations';
import Container from 'pages/components/base/Container';
import MasterLayout from 'pages/layouts/MasterLayout';
import GoogleMap from './components/base/MyGoogleMap';

function IndexPage() {
  useInitSocketIO();
  return (
    <Container style={{ position: 'relative' }}>
      <>
        <GoogleMap apiKey="AIzaSyAzT63S9denkIRaQ4-FrtzNAiB7haolovg" />
      </>
    </Container>
  );
}

IndexPage.Layout = MasterLayout;

export const getServerSideProps = withSSTranslations();

export default IndexPage;
