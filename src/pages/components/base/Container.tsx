import { CSSProperties } from 'react';

type Props = {
  children: JSX.Element;
  style?: CSSProperties;
};

function Container(props: Props) {
  const { children, style } = props;
  return (
    <div style={{ padding: 0, margin: '0 auto', ...style }} className="container">
      {children}
    </div>
  );
}

export default Container;
