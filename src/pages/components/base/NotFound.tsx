import { HomeOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import Title from 'antd/lib/typography/Title';
import Link from 'next/link';
import ElementCenter from 'pages/components/base/ElementCenter';
import React from 'react';
import { useTranslation } from 'react-i18next';

const NotFound = () => {
  const { t } = useTranslation();
  return (
    <ElementCenter>
      <div style={{ textAlign: 'center' }}>
        <Title>404 | Not found</Title>
        <Button type="primary">
          <Link href="/">
            {<HomeOutlined />} {t('back_home')}
          </Link>
        </Button>
      </div>
    </ElementCenter>
  );
};

export default NotFound;
