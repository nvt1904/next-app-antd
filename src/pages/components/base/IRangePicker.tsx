import { DatePicker } from 'antd';
import { RangePickerProps } from 'antd/es/date-picker/generatePicker';
import useLocale from 'hooks/base/locale/useLocale';
import { Moment } from 'moment';
import React from 'react';

const { RangePicker } = DatePicker;

function IRangePicker(props: RangePickerProps<Moment>) {
  const { locale } = useLocale();
  return <RangePicker format={locale.DatePicker?.lang.dateFormat} {...props} />;
}

export default IRangePicker;
