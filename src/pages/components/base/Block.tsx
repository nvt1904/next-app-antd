import { ArrowLeftOutlined } from '@ant-design/icons';
import { Button, Card, Col, Row } from 'antd';
import { useRouter } from 'next/router';
import React from 'react';

type BloclProps = {
  title: JSX.Element | string;
  onBack?: boolean;
  children: JSX.Element | React.ReactNode | React.ReactNodeArray;
  actions?: JSX.Element | React.ReactNode | React.ReactNodeArray;
};

function Block(props: BloclProps) {
  const { title, onBack, children, actions } = props;
  const router = useRouter();
  const handleOnBack = () => {
    onBack && router.back();
  };
  const btnOnBack = () => {
    if (onBack) {
      return (
        <Button shape="circle" onClick={handleOnBack} type="text">
          <ArrowLeftOutlined />
        </Button>
      );
    } else {
      return undefined;
    }
  };
  return (
    <Card
      title={
        <Row style={{ padding: '0 .5em' }} gutter={[8, 0]} justify="space-between">
          <Col>
            {btnOnBack()}
            {title}
          </Col>
          <Col>{actions}</Col>
        </Row>
      }
      bordered={false}
      style={{ width: '100%' }}
      headStyle={{ padding: '0' }}
      bodyStyle={{ padding: '1em .5em' }}>
      {children}
    </Card>
  );
}

export default Block;
