import { RowProps } from 'antd/lib/row';

interface Props extends RowProps {
  children: JSX.Element;
}

const ElementCenter = (props: Props) => {
  const { children, ...reset } = props;
  return (
    <div
      {...reset}
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        ...reset?.style
      }}>
      {children}
    </div>
  );
};

export default ElementCenter;
