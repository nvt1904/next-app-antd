import { FullscreenExitOutlined, FullscreenOutlined } from '@ant-design/icons';
import { useFullscreen } from 'ahooks';
import { Button } from 'antd';
import { ButtonProps } from 'antd/lib/button';
import React from 'react';

interface Props extends ButtonProps {
  targetId?: string;
}

function ToggleFullScreen(props: Props) {
  const { targetId, ...reset } = props;
  const [isFullscreen, { toggleFull }] = useFullscreen(() =>
    targetId ? window.document.getElementById(targetId) : window.document.body
  );

  return (
    <Button
      onClick={() => toggleFull()}
      type="text"
      icon={isFullscreen ? <FullscreenExitOutlined /> : <FullscreenOutlined />}
      shape={'circle'}
      {...reset}
    />
  );
}

export default ToggleFullScreen;
