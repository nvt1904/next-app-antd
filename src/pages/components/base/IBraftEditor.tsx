import { Card } from 'antd';
import BraftEditor, { BraftEditorProps } from 'braft-editor';
import 'braft-editor/dist/index.css';
import React, { useEffect, useState } from 'react';

type Props = BraftEditorProps & {
  value?: string;
  onChange?: (value: string) => void;
};

function IBraftEditor(props: Props) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { value, onChange, ...reset } = props;
  const [editor, setEditor] = useState(BraftEditor.createEditorState(null));

  useEffect(() => {
    if (props?.value !== editor.toHTML()) {
      setEditor(BraftEditor.createEditorState(props?.value));
    }
  }, [props?.value]);

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleChange = (editorState: any) => {
    setEditor(editorState);
  };

  const handleSave = () => {
    props?.onChange && props?.onChange(editor.toHTML());
  };

  return (
    <Card bodyStyle={{ padding: 0 }}>
      <BraftEditor
        language="en"
        value={editor}
        onBlur={handleSave}
        onChange={handleChange}
        {...reset}
      />
    </Card>
  );
}

export default IBraftEditor;
