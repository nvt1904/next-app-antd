import { Table } from 'antd';
import { TableProps } from 'antd/lib/table';
import { merge } from 'lodash';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Loading from './Loading';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
interface Props extends TableProps<any> {
  headerExtra?: JSX.Element;
}

function MyTable(props: Props) {
  const { loading, headerExtra, ...reset } = props;
  const { t } = useTranslation();
  return (
    <>
      {headerExtra ? (
        <div
          style={{
            padding: '1em .5em'
          }}>
          {headerExtra}
        </div>
      ) : (
        ''
      )}
      <Table
        {...merge(
          {
            rowKey: 'id',
            loading: {
              spinning: Boolean(loading),
              indicator: <Loading />
            },
            pagination: {
              showTotal: (total: number, range: number[]) =>
                t('pagination_info', { range: `${range[0]} - ${range[1]}`, total }),
              position: ['bottomCenter'],
              showSizeChanger: true,
              showQuickJumper: true,
              responsive: true
            },
            sticky: true,
            scroll: { x: 720, y: 1366 }
          },
          reset
        )}
      />
    </>
  );
}

export default MyTable;
