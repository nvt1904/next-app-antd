import { LoadingOutlined, SearchOutlined } from '@ant-design/icons';
import { Input } from 'antd';
import { InputProps } from 'antd/lib/input';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDebounceFn } from 'ahooks';

interface Props extends InputProps {
  loading?: boolean;
  onSearch: (value: string) => void;
  wait?: number;
}

export default function InputSearch(props: Props) {
  const { loading, onSearch, wait, ...reset } = props;
  const { t } = useTranslation();
  const { run } = useDebounceFn(
    (value: string) => {
      onSearch(value);
    },
    {
      wait: (wait ? (wait > 0 ? wait : 0) : 0) || 500
    }
  );
  return (
    <Input
      readOnly={loading}
      placeholder={t('input_search')}
      onChange={(e) => run(e.target.value)}
      suffix={loading ? <LoadingOutlined /> : <SearchOutlined />}
      {...reset}
    />
  );
}
