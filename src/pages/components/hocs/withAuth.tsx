import { useSession } from 'next-auth/client';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import Loading from '../base/Loading';

// eslint-disable-next-line react/display-name
export const withAuth = (WrapperComponent) => (props) => {
  const router = useRouter();
  const [session, loading] = useSession();
  useEffect(() => {
    if (!loading) {
      if (!session) {
        router.push({ pathname: '/auth/login', query: { callbackUrl: router.asPath } });
      }
    }
  }, [loading]);

  if (session) {
    return <WrapperComponent {...props} />;
  }
  return <Loading />;
};
