import { MailOutlined, SettingOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import { MenuProps } from 'antd/lib/menu';

const { SubMenu, Item, ItemGroup } = Menu;

type Props = MenuProps;

function MenuHeader(props: Props) {
  const { mode, ...reset } = props;
  return (
    <Menu mode={mode || 'horizontal'} {...reset}>
      <Item key="mail" icon={<MailOutlined />}>
        Navigation One
      </Item>
      <SubMenu icon={<SettingOutlined />} title="Navigation Three - Submenu">
        <ItemGroup title="Item 1">
          <Item key="setting:1">Option 1</Item>
          <Item key="setting:2">Option 2</Item>
        </ItemGroup>
        <ItemGroup title="Item 2">
          <Item key="setting:3">Option 3</Item>
          <Item key="setting:4">Option 4</Item>
        </ItemGroup>
      </SubMenu>
      <Item key="alipay">
        <a href="https://ant.design" target="_blank" rel="noopener noreferrer">
          Navigation Four - Link
        </a>
      </Item>
    </Menu>
  );
}

export default MenuHeader;
