import { Button, Card, Col, Form, Input, Row, Space, Typography } from 'antd';
import useLogin, { DataLogin } from 'hooks/base/auth/useLogin';
import { withSSTranslations } from 'middleware/base/withSSTranslations';
import { withSSUnAuth } from 'middleware/base/withSSUnAuth';
import { useSession } from 'next-auth/client';
import { useRouter } from 'next/router';
import ElementCenter from 'pages/components/base/ElementCenter';
import { RootState } from 'pages/store';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

const LoginPage = ({ query }) => {
  const { t } = useTranslation();
  const router = useRouter();
  const [session, sessionLoading] = useSession();
  const { logging, login, result } = useLogin();
  const { loading } = useSelector((state: RootState) => state.nprogress);

  useEffect(() => {
    if (!sessionLoading) {
      if (session) {
        router.replace(query?.callbackUrl || '/');
      }
    }
  }, [session, sessionLoading]);

  const onFinish = async (values: DataLogin) => {
    login(
      {
        username: values.username,
        password: values.password
      },
      'credentials'
    );
  };

  return (
    <ElementCenter style={{ height: '100vh' }}>
      <Row justify="center" style={{ width: '100%' }}>
        <Col md={6} sm={12} xs={24}>
          <Card headStyle={{ textAlign: 'center', width: '100%' }} title={t('login')}>
            <Form layout="vertical" onFinish={onFinish}>
              <Form.Item
                label={t('form:labels.username')}
                name="username"
                rules={[{ required: true }]}>
                <Input />
              </Form.Item>
              <Form.Item
                label={t('form:labels.password')}
                name="password"
                rules={[{ required: true }]}>
                <Input.Password />
              </Form.Item>
              <Form.Item>
                <Space direction="vertical">
                  <Typography.Text type="danger">{result?.error}</Typography.Text>
                  <Button loading={logging || loading} type="primary" htmlType="submit">
                    {t('form:labels.login')}
                  </Button>
                </Space>
              </Form.Item>
            </Form>
          </Card>
        </Col>
      </Row>
    </ElementCenter>
  );
};

export const getServerSideProps = withSSUnAuth(
  withSSTranslations(['form'], async (ctx) => ({
    props: {
      query: ctx.query
    }
  }))
);

export default LoginPage;
