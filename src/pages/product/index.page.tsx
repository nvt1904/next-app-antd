import { DatePicker } from 'antd';
import { withSSTranslations } from 'middleware/base/withSSTranslations';
import { useTranslation } from 'next-i18next';
import dynamic from 'next/dynamic';
import Link from 'next/link';
import Container from 'pages/components/base/Container';
import MasterLayout from 'pages/layouts/MasterLayout';

const ChangeLocale = dynamic(() => import('pages/components/base/ChangeLocale'), { ssr: false });

function IndexProductPage() {
  const { t } = useTranslation();
  return (
    <Container style={{ position: 'relative' }}>
      <MasterLayout>
        <Link href="/">{t('home')}</Link>
        <Link href="/admin">{t('admin')}</Link>
        <br />
        <DatePicker />
        <br />
        <ChangeLocale />
      </MasterLayout>
    </Container>
  );
}

export const getServerSideProps = withSSTranslations();

export default IndexProductPage;
