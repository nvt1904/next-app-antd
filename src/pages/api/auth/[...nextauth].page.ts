import axiosClient from 'helper/axiosClient';
import moment from 'moment';
import NextAuth from 'next-auth';
import { signOut } from 'next-auth/client';
import Providers from 'next-auth/providers';
import { pick } from 'lodash';

type AuthCredentials = { username: string; password: string };

export default (req, res) => {
  return NextAuth(req, res, {
    providers: [
      Providers.Credentials({
        id: 'credentials',
        name: 'credentials',
        credentials: {
          username: { label: 'Username', type: 'text' },
          password: { label: 'Password', type: 'password' }
        },
        async authorize(credentials: AuthCredentials) {
          const res = await axiosClient.post('auth/login', credentials);
          if (res.error) {
            throw new Error(String(res?.message));
          }
          return res;
        }
      })
    ],
    pages: {
      signIn: '/auth/login'
    },
    session: {
      jwt: true
    },
    callbacks: {
      async jwt(token, user) {
        if (user) {
          return { ...token, ...user };
        }
        if (moment(token?.expires) <= moment()) {
          const res = await axiosClient.post('/auth/refresh-token', {
            refreshToken: token?.refreshToken
          });
          if (res.error) {
            return signOut();
          }
          return { ...token, ...res };
        }
        return token;
      },
      async session(session, userOrToken) {
        session.user = pick(userOrToken, ['accessToken', 'refreshToken', 'expires']);
        return session;
      }
    }
  });
};
