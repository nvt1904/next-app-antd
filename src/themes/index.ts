import dark from 'themes/dark';
import defaultTheme from 'themes/default';

export default { dark, default: defaultTheme };
