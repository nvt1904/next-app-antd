/* eslint-disable @typescript-eslint/no-explicit-any */
import { message } from 'antd';
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import statusCode from 'http-status-codes';
import { cloneDeep } from 'lodash';
import moment from 'moment';
import { getSession } from 'next-auth/client';
import queryString from 'query-string';

export type Params = {
  limit?: number;
  page?: number;
  [key: string]: any;
};

export type Data = {
  [key: string]: any;
};

export type IRequest = {
  url: string;
  params: Params;
  data: Data;
};
export type IResponse = {
  message?: string;
  items?: any[];
  error?: string;
  [key: string]: any;
};

export const getHeader = async (headers: any = {}) => {
  if (headers) {
    const session = await getSession();
    if (session) {
      const accessToken = session.user?.accessToken;
      if (accessToken) {
        return {
          ...headers,
          Authorization: accessToken
        };
      }
    }
    return { ...headers };
  }
};

export const instance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_BASE_API_URL,
  timeout: 10000,
  paramsSerializer: (params) => queryString.stringify(params)
});

const requestHandler = (request: AxiosRequestConfig) => {
  console.log(
    `${moment().format('YYYY-MM-DD HH:mm:ss:SSS')} >>> ${request.method.toUpperCase()} ${
      request.url
    }`
  );
  console.log(`+ Params `, request.params);
  console.log(`+ Data   `, request.data);
  return request;
};

const successHandler = (response: AxiosResponse) => {
  console.log(
    `${moment().format('YYYY-MM-DD HH:mm:ss:SSS')} <<< ${response.config.method.toUpperCase()} ${
      response?.config?.url
    }`,
    response.data
  );
  return response.data;
};

const errorHandler = async (error: AxiosError) => {
  if (error?.response?.status) {
    if (process.browser) {
      message.error(error?.response?.data?.message || error.message);
    }
    return { error: error, message: error.message, ...cloneDeep(error?.response?.data) };
  }
  return { message: error.message, error: error };
};

instance.interceptors.request.use((request: AxiosRequestConfig) => {
  return requestHandler(request);
});

instance.interceptors.response.use(
  (response: AxiosResponse) => {
    return successHandler(response);
  },
  (error: AxiosError) => {
    return errorHandler(error);
  }
);

const axiosClient = {
  get: async <ReqType, ResType extends IResponse>(
    url: string,
    params?: ReqType,
    headers: any = {}
  ): Promise<ResType> => {
    headers = await getHeader(headers);
    return instance.get(url, { params, headers });
  },
  post: async <ReqType, ResType extends IResponse>(
    url: string,
    data?: ReqType,
    headers: any = {}
  ): Promise<ResType> => {
    headers = await getHeader(headers);
    return instance.post(url, data, { headers });
  },
  put: async <ReqType, ResType extends IResponse>(
    url: string,
    data?: ReqType,
    headers: any = {}
  ): Promise<ResType> => {
    headers = await getHeader(headers);
    return instance.put(url, data, { headers });
  },
  delete: async <ReqType, ResType extends IResponse>(
    url: string,
    params?: ReqType,
    headers: any = {}
  ): Promise<ResType> => {
    headers = await getHeader(headers);
    return instance.delete(url, { params, headers });
  },
  statusCode: statusCode
};

export default axiosClient;
