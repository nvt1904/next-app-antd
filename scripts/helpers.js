const _ = require('lodash');

const objectMultiToOneDepth = (data, prefix = '') => {
  const dataRaw = { ...data };
  let newData = {};
  for (const key in dataRaw) {
    if (typeof dataRaw[key] === 'object') {
      newData = _.merge(newData, objectMultiToOneDepth(dataRaw[key], `${prefix}${key}.`));
    } else {
      newData[`${prefix}${key}`] = dataRaw[key];
    }
  }
  return newData;
};

const objectOneToMultiDepth = (data) => {
  const dataRaw = { ...data };
  let newData = {};
  function fnHelper(keys, value) {
    const newObject = {};
    if (keys.length > 0) {
      const keyFirst = keys.shift();
      if (keyFirst) {
        if (keys.length > 0) {
          newObject[keyFirst] = fnHelper(keys, value);
        } else {
          newObject[keyFirst] = value;
        }
      }
    }
    return newObject;
  }
  for (const key in dataRaw) {
    const keys = key.split('.');
    newData = { ..._.merge(fnHelper(keys, dataRaw[key]), newData) };
  }
  return newData;
};

const sortObjectBykey = (data) => {
  const dataRaw = { ...data };
  const newData = {};
  _(dataRaw)
    .keys()
    .sort()
    .each(function (key) {
      if (typeof dataRaw[key] === 'object') {
        newData[key] = sortObjectBykey(dataRaw[key]);
      } else {
        newData[key] = dataRaw[key];
      }
    });
  return newData;
};

module.exports = {
  objectMultiToOneDepth,
  objectOneToMultiDepth,
  sortObjectBykey
};
