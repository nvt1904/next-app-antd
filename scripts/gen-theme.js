const fs = require('fs');
const path = require('path');
const { getLessVars } = require('antd-theme-generator');

const themeVariables = getLessVars('./src/themes/variables.less');
fs.writeFileSync(
  path.resolve('./src/themes/variables.ts'),
  `const themeVariables = ${JSON.stringify(themeVariables)}; export default themeVariables;`
);

const defaultVars = {
  ...getLessVars('./node_modules/antd/es/style/themes/default.less'),
  ...themeVariables
};
fs.writeFileSync(
  path.resolve('./src/themes/default.ts'),
  `const defaultTheme = ${JSON.stringify(defaultVars)}; export default defaultTheme;`
);

const darkVars = {
  ...defaultVars,
  ...getLessVars('./node_modules/antd/es/style/themes/dark.less')
};
fs.writeFileSync(
  path.resolve('./src/themes/dark.ts'),
  `const darkTheme = ${JSON.stringify(darkVars)}; export default darkTheme;`
);
