import { Session } from 'next-auth';
import moment from 'moment';
import { JWT } from 'next-auth/jwt';

interface User {
  accessToken: string;
  refreshToken: string;
  expires: string;
}

/** Example on how to extend the built-in session types */
declare module 'next-auth' {
  interface Session {
    user: User;
  }
}

/** Example on how to extend the built-in types for JWT */
declare module 'next-auth/jwt' {
  interface JWT extends User {}
}
